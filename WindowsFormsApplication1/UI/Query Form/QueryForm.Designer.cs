﻿namespace QueryFormApp
{
    partial class QueryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SearchButton = new System.Windows.Forms.Button();
            this.DisciplineLabel = new System.Windows.Forms.Label();
            this.SubDisciplineLabel = new System.Windows.Forms.Label();
            this.DisciplineComboBox = new System.Windows.Forms.ComboBox();
            this.SubDisciplineComboBox = new System.Windows.Forms.ComboBox();
            this.DeliverableListBox = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.DeliverableListBox)).BeginInit();
            this.SuspendLayout();
            // 
            // SearchButton
            // 
            this.SearchButton.AccessibleName = "Search";
            this.SearchButton.Location = new System.Drawing.Point(378, 69);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(135, 32);
            this.SearchButton.TabIndex = 0;
            this.SearchButton.Text = "Search\r\n";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // DisciplineLabel
            // 
            this.DisciplineLabel.AutoSize = true;
            this.DisciplineLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DisciplineLabel.Location = new System.Drawing.Point(30, 25);
            this.DisciplineLabel.Name = "DisciplineLabel";
            this.DisciplineLabel.Size = new System.Drawing.Size(71, 18);
            this.DisciplineLabel.TabIndex = 1;
            this.DisciplineLabel.Text = "Discipline";
            // 
            // SubDisciplineLabel
            // 
            this.SubDisciplineLabel.AutoSize = true;
            this.SubDisciplineLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubDisciplineLabel.Location = new System.Drawing.Point(30, 75);
            this.SubDisciplineLabel.Name = "SubDisciplineLabel";
            this.SubDisciplineLabel.Size = new System.Drawing.Size(102, 18);
            this.SubDisciplineLabel.TabIndex = 2;
            this.SubDisciplineLabel.Text = "Sub-Discipline";
            // 
            // DisciplineComboBox
            // 
            this.DisciplineComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DisciplineComboBox.FormattingEnabled = true;
            this.DisciplineComboBox.Items.AddRange(new object[] {
            "Pipelines",
            "Flow Assurance",
            "Systems"});
            this.DisciplineComboBox.Location = new System.Drawing.Point(145, 28);
            this.DisciplineComboBox.Name = "DisciplineComboBox";
            this.DisciplineComboBox.Size = new System.Drawing.Size(209, 26);
            this.DisciplineComboBox.TabIndex = 3;
            // 
            // SubDisciplineComboBox
            // 
            this.SubDisciplineComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubDisciplineComboBox.FormattingEnabled = true;
            this.SubDisciplineComboBox.Items.AddRange(new object[] {
            "Buckling",
            "Routing",
            "Installation",
            "Steady State Analysis",
            "Transient Analysis",
            "Data",
            "Structures"});
            this.SubDisciplineComboBox.Location = new System.Drawing.Point(145, 75);
            this.SubDisciplineComboBox.Name = "SubDisciplineComboBox";
            this.SubDisciplineComboBox.Size = new System.Drawing.Size(209, 26);
            this.SubDisciplineComboBox.TabIndex = 4;
            // 
            // DeliverableListBox
            // 
            this.DeliverableListBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DeliverableListBox.Location = new System.Drawing.Point(12, 126);
            this.DeliverableListBox.Name = "DeliverableListBox";
            this.DeliverableListBox.Size = new System.Drawing.Size(501, 259);
            this.DeliverableListBox.TabIndex = 5;
            // 
            // QueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 397);
            this.Controls.Add(this.DeliverableListBox);
            this.Controls.Add(this.SubDisciplineComboBox);
            this.Controls.Add(this.DisciplineComboBox);
            this.Controls.Add(this.SubDisciplineLabel);
            this.Controls.Add(this.DisciplineLabel);
            this.Controls.Add(this.SearchButton);
            this.Name = "QueryForm";
            this.Text = "Query Form";
            ((System.ComponentModel.ISupportInitialize)(this.DeliverableListBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Label DisciplineLabel;
        private System.Windows.Forms.Label SubDisciplineLabel;
        private System.Windows.Forms.ComboBox DisciplineComboBox;
        private System.Windows.Forms.ComboBox SubDisciplineComboBox;
        private System.Windows.Forms.DataGridView DeliverableListBox;
    }
}

