﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QueryFormApp
{
    public partial class QueryForm : Form
    {
        public QueryForm()
        {
            InitializeComponent();
        }


        //stores the value from the discipline combo box into a variable       
        public string Discipline
        {
            get { return this.DisciplineComboBox.Text; }
            set { this.DisciplineComboBox.Text = value; }
        }


        //stores the value from the sub-discipline combo box into a variable
        public string SubDiscipline
        {
            get { return this.SubDisciplineComboBox.Text; }
            set { this.SubDisciplineComboBox.Text = value; }
        }
        //event triggered when search is clicked
        //connection to sql server is made, and the query is raised based on the filters in the discipline and sub-discipline combo box.
        //
        private void SearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                String str = "server=UKLDCWPSQL13V;database=CTRSelector;Integrated Security=True";
                String query = "Select Deliverable from [dbo].[QueryTable] where Discipline = '" + Discipline + "' and SubDiscipline = '" + SubDiscipline + "'";
                SqlConnection con = new SqlConnection(str);
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();

                //this codeblock is for listing out the deliverables in a DataGridView.
                
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmd;
                DataTable DeliverableDataset = new DataTable();
                sda.Fill(DeliverableDataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = DeliverableDataset;
                DeliverableListBox.DataSource = bSource;
                sda.Update(DeliverableDataset);

                //This code block is for storing the values in a list and using a list View for display.

                //DataSet ds = new DataSet();
                //SqlDataReader DeliverableReader;
                //DeliverableReader = cmd.ExecuteReader();
                //var DeliverableList = new List<string>();
                //while (DeliverableReader.Read())
                //{
                //    string Deliverable = (string)DeliverableReader["Deliverable"];
                //    //DeliverableList.Add(Deliverable);
                //    MessageBox.Show(Deliverable);
                //}

                
           
                con.Close();
                
            }



            catch (Exception es)
            {
                MessageBox.Show(es.Message);

            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

    }
}
